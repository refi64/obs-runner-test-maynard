maynard (0.2022.14) apertis; urgency=medium

  * shell-app-system: provide proper app_id.
  * shell-app-system: provide a function for changing the app-id.
  * launcher: add public function for launching apps.
  * maynard: launch favorites using launcher.
  * launcher: update app-id when different from the desktop file.
  * maynard: activate last app when another one quits.
  * maynard: fix launcher visibility.
  * Get rid of compilation warnings.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Mon, 17 Jan 2022 12:19:17 +0100

maynard (0.2022.13) apertis; urgency=medium

  * Set XDG_MENU_PREFIX instead of relying on libgnome-menu

 -- Ryan Gonzalez <ryan.gonzalez@collabora.com>  Mon, 27 Dec 2021 11:38:00 -0600

maynard (0.2022.12) apertis; urgency=medium

  * Fix application ID logic used to re-activate a running app instance

 -- Ryan Gonzalez <ryan.gonzalez@collabora.com>  Mon, 29 Nov 2021 07:18:00 -0600

maynard (0.2022.11) apertis; urgency=medium

  * Remove dependency on libgnome-desktop-3

 -- Ryan Gonzalez <ryan.gonzalez@collabora.com>  Wed, 27 Oct 2021 01:06:00 -0600

maynard (0.2022.10) apertis; urgency=medium

  * Remove debian/apertis/gitlab-ci.yml
  * Add debian/apertis/copyright

 -- Vignesh Raman <vignesh.raman@collabora.com>  Mon, 11 Oct 2021 09:22:24 +0530

maynard (0.2022.9) apertis; urgency=medium

  * agl-compositor.ini: don't require input device presence on startup.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Wed, 21 Jul 2021 13:21:30 +0200

maynard (0.2022.8) apertis; urgency=medium

  * Add default menu file
  * Drop recommends on gnome-menus

 -- Walter Lozano <walter.lozano@collabora.com>  Sat, 26 Jun 2021 07:42:48 +0000

maynard (0.2022.7) apertis; urgency=medium

  * Fix incorrect argument declaration

 -- Walter Lozano <walter.lozano@collabora.com>  Wed, 16 Jun 2021 18:55:38 +0000

maynard (0.2022.6) apertis; urgency=medium

  * Remove canterbury support
  * Upgrade to libweston-9

 -- Walter Lozano <walter.lozano@collabora.com>  Thu, 15 Apr 2021 22:44:09 -0300

maynard (0.2022.5) apertis; urgency=medium

  * client/maynard: Add simple app lifecycle checks
  * client/maynard: Use agl-compositor for app lifecycle notifications

 -- Walter Lozano <walter.lozano@collabora.com>  Wed, 17 Feb 2021 15:47:50 -0300

maynard (0.2022.4) apertis; urgency=medium

  * client/shell-app-system: Avoid showing applications with NoDisplay key

 -- Walter Lozano <walter.lozano@collabora.com>  Wed, 20 Jan 2021 11:49:18 +0000

maynard (0.2022.3) apertis; urgency=medium

  * client/maynard: Remove debug trace
  * client/maynard: Configure indicators_menu as popup
  * client/maynard: Avoid deactivating when activating a different surface

 -- Walter Lozano <walter.lozano@collabora.com>  Wed, 18 Nov 2020 18:27:12 +0000

maynard (0.2022.2) apertis; urgency=medium

  * debian/control: Add libcanterbury-platform as dependency

 -- Walter Lozano <user@apertis>  Tue, 10 Nov 2020 22:36:30 +0000

maynard (0.2022.1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * Set component to target

  [ Walter Lozano ]
  * client/maynard: Add signal handler
  * client/maynard: Fix canterbury support
  * debian/control: Add gnome-menus and canterbury dependencies
  * debian/control: Improve readability and maintainability
  * maynard/client: Improve readability by defining panel preferred width
  * client/maynard: Explicitly set launcher window size
  * client/maynard: Explicitly set indicators_menus window size
  * client/shell-app-system: Remove specific calls to canterbury

 -- Walter Lozano <walter.lozano@collabora.com>  Mon, 09 Nov 2020 15:56:41 +0000

maynard (0.2022.0) apertis; urgency=low

  * Initial release

 -- Walter Lozano <walter.lozano@collabora.com>  Wed, 23 Sep 2020 16:03:36 -0300
