/*
 * Copyright (C) 2019 Collabora Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

#include "config.h"

#include "clock-indicator.h"

#include <glib-unix.h>
#include <sys/timerfd.h>

#define LOCALTIME_PATH "/etc/localtime"

#define USEC_PER_SEC 1000000

struct _MaynardClockIndicatorPrivate {
  GtkWidget *label;
  GtkWidget *menu_label_weekday;
  GtkWidget *menu_label_date;

  guint timer_source;
  int timer_fd;
  GFileMonitor *tz_monitor;
};

G_DEFINE_TYPE_WITH_PRIVATE (MaynardClockIndicator, maynard_clock_indicator, MAYNARD_INDICATOR_TYPE)

static void
maynard_clock_indicator_init (MaynardClockIndicator *self)
{
  self->priv = maynard_clock_indicator_get_instance_private (self);
  self->priv->timer_fd = -1;
}

static gboolean
update_time (MaynardClockIndicator *self);

static gboolean
on_timer_event (int fd,
    GIOCondition io_condition,
    gpointer user_data)
{
  MaynardClockIndicator *self = MAYNARD_CLOCK_INDICATOR (user_data);
  gint64 expirations;

  g_return_val_if_fail (self != NULL, G_SOURCE_REMOVE);

  if (io_condition & ~(G_IO_IN | G_IO_PRI))
    {
      g_warning ("Unexpected io conditions on clock timer: %x\n", io_condition);
      return G_SOURCE_REMOVE;
    }

  if (read (fd, &expirations, sizeof(expirations)) == -1 && errno != ECANCELED)
    {
      g_warning ("Failed to read from clock timer fd: %s", strerror (errno));
      return G_SOURCE_REMOVE;
    }

  if (!update_time (self))
    return G_SOURCE_REMOVE;

  return G_SOURCE_CONTINUE;
}

static void
clear_timer (MaynardClockIndicator *self)
{
  if (self->priv->timer_source > 0)
    {
      g_source_remove (self->priv->timer_source);
      self->priv->timer_source = 0;
    }

  if (self->priv->timer_fd != -1)
    {
      close (self->priv->timer_fd);
      self->priv->timer_fd = -1;
    }
}

static gboolean
create_timer (MaynardClockIndicator *self)
{
  g_return_val_if_fail (self->priv->timer_fd == -1, FALSE);

  self->priv->timer_fd = timerfd_create (CLOCK_REALTIME, TFD_CLOEXEC);
  if (self->priv->timer_fd == -1)
    {
      g_warning ("Failed to create clock timer: %s", strerror (errno));
      return FALSE;
    }

  self->priv->timer_source = g_unix_fd_add_full (G_PRIORITY_HIGH,
      self->priv->timer_fd, G_IO_IN, on_timer_event, self, NULL);

  return TRUE;
}

static gboolean
arm_timer (MaynardClockIndicator *self,
    GDateTime *now)
{
  GDateTime *next_minute;
  GTimeSpan usec_to_next_minute = 0;
  struct itimerspec ts = {0};

  g_return_val_if_fail (self->priv->timer_fd != -1, FALSE);

  usec_to_next_minute = (60 - g_date_time_get_second (now)) * USEC_PER_SEC
      - g_date_time_get_microsecond (now);
  next_minute = g_date_time_add (now, usec_to_next_minute);

  ts.it_value.tv_sec = g_date_time_to_unix (next_minute);
  g_date_time_unref (next_minute);

  /* timerfd_settime can return ECANCELED even if the time was successfully set,
     see NOTES in timerfd_create(2). */
  if (timerfd_settime (self->priv->timer_fd,
          TFD_TIMER_ABSTIME | TFD_TIMER_CANCEL_ON_SET, &ts, NULL) == -1
      && errno != ECANCELED)
    {
      g_warning ("Failed to arm clock timer: %s", strerror (errno));
      return FALSE;
    }

  return TRUE;
}

static gboolean
update_time (MaynardClockIndicator *self)
{
  GDateTime *datetime;
  gchar *str;
  gboolean success;

  datetime = g_date_time_new_now_local ();

  str = g_date_time_format (datetime, "%H:%M");
  gtk_label_set_markup (GTK_LABEL (self->priv->label), str);
  g_free (str);

  str = g_date_time_format (datetime, "%A");
  gtk_label_set_text (GTK_LABEL (self->priv->menu_label_weekday), str);
  g_free (str);

  str = g_date_time_format (datetime, "%e %B %Y");
  gtk_label_set_text (GTK_LABEL (self->priv->menu_label_date), str);
  g_free (str);

  success = arm_timer (self, datetime);

  g_date_time_unref (datetime);
  return success;
}

static void
on_timezone_change (GFileMonitor *monitor,
    GFile *file,
    GFile *other_file,
    GFileMonitorEvent event_type,
    gpointer user_data)
{
  MaynardClockIndicator *self = user_data;

  if (event_type == G_FILE_MONITOR_EVENT_CHANGES_DONE_HINT)
    update_time (self);
}

static void
track_timezone_changes (MaynardClockIndicator *self)
{
  GFile *localtime_file;
  GError *error = NULL;

  localtime_file = g_file_new_for_path (LOCALTIME_PATH);
  self->priv->tz_monitor = g_file_monitor_file (
      localtime_file, G_FILE_MONITOR_NONE, NULL, &error);
  if (self->priv->tz_monitor == NULL)
    {
      g_warning ("Failed to set up monitor for %s: %s",
          LOCALTIME_PATH, error->message);
      g_clear_error (&error);
    }
  else
    {
      g_signal_connect (self->priv->tz_monitor, "changed",
          G_CALLBACK (on_timezone_change), self);
    }

  g_object_unref (localtime_file);
}

static GtkWidget *
create_clock_menu (MaynardClockIndicator *self)
{
  GtkWidget *box;
  GtkWidget *label_weekday;
  GtkWidget *label_date;

  box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  gtk_style_context_add_class (gtk_widget_get_style_context (box),
      "maynard-clock-indicator-menu-item");

  label_weekday = gtk_label_new ("");
  gtk_label_set_justify (GTK_LABEL (label_weekday), GTK_JUSTIFY_LEFT);
  gtk_label_set_xalign (GTK_LABEL (label_weekday), 0);
  gtk_style_context_add_class (gtk_widget_get_style_context (label_weekday),
      "maynard-clock-indicator-menu-item-weekday");
  gtk_box_pack_start (GTK_BOX (box), label_weekday, FALSE, FALSE, 0);
  self->priv->menu_label_weekday = label_weekday;

  label_date = gtk_label_new ("");
  gtk_label_set_justify (GTK_LABEL (label_date), GTK_JUSTIFY_LEFT);
  gtk_label_set_xalign (GTK_LABEL (label_date), 0);
  gtk_style_context_add_class (gtk_widget_get_style_context (label_date),
      "maynard-clock-indicator-menu-item-date");
  gtk_box_pack_start (GTK_BOX (box), label_date, FALSE, FALSE, 0);
  self->priv->menu_label_date = label_date;

  return box;
}

static void
maynard_clock_indicator_constructed (GObject *object)
{
  MaynardClockIndicator *self = MAYNARD_CLOCK_INDICATOR (object);
  GtkWidget *label, *menu;

  G_OBJECT_CLASS (maynard_clock_indicator_parent_class)->constructed (object);

  gtk_style_context_add_class (
      gtk_widget_get_style_context (GTK_WIDGET (self)),
      "maynard-clock-indicator");

  label = gtk_label_new ("");
  gtk_style_context_add_class (gtk_widget_get_style_context (label),
      "maynard-clock-indicator-label");
  self->priv->label = label;
  maynard_indicator_set_widget (MAYNARD_INDICATOR (self), label);

  /* the menu */
  menu = create_clock_menu (self);
  maynard_indicator_set_menu (MAYNARD_INDICATOR (self), menu);

  if (create_timer (self))
    {
      track_timezone_changes (self);
      update_time (self);
    }
}

static void
maynard_clock_indicator_dispose (GObject *object)
{
  MaynardClockIndicator *self = MAYNARD_CLOCK_INDICATOR (object);

  g_clear_object (&self->priv->tz_monitor);
  clear_timer (self);

  G_OBJECT_CLASS (maynard_clock_indicator_parent_class)->dispose (object);
}

static void
maynard_clock_indicator_class_init (MaynardClockIndicatorClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)klass;

  object_class->constructed = maynard_clock_indicator_constructed;
  object_class->dispose = maynard_clock_indicator_dispose;
}

GtkWidget *
maynard_clock_indicator_new (void)
{
  return g_object_new (MAYNARD_CLOCK_INDICATOR_TYPE, NULL);
}
